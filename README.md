Instructions:

* Download a zip file of the repository
* Implement a simple search function according to wireframes and UI (attached in source)
* The search should use a public REST API of your choice using JavaScript
* Search for title, return title (or something like that)
* Display partial search results in a list beneath the search field
* When hitting [ENTER] the selected search value should be saved with date/timestamp beneath the search box (as a search history)
* The page should be responsive, so that if width of window is changed it should adapt
* Nothing needs to be saved if the page is reloaded (i.e don�t have to use cookies/database etc)
* Logo and search-icon is provided under /assets
* We expect a total of 3 files - .html, .js and .css
* Solutions built using frameworks such as flex-grid, bootstrap or similar will be discarded.
 
What we look at
* HTML5 semantics, complexity of css3 solutions and js sanity.
* General sanity check on structure and solution
 
Solve the task as far as you think is necessary.
 
When done send a .zip file to admin@anthood.com containing your project.
